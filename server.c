#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>

#define PORT 8080

// client connection structure
typedef struct {
	struct sockaddr_in addr;	/* Client remote address */
	int connection_fd;			/* Connection file descriptor */
	int uid;			/* Client unique identifier */
	char name[32];			/* Client name */
	int login;
} client_t;


int main(int argc, char *argv[]){
	struct sockaddr_in server_addr;
	int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(PORT);

	if(bind(socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0){
		perror("Socket binding failed");
		return 1;
	}

	if(listen(socket_fd, 20) < 0){
		perror("Socket listening failed");
		return 1;
	}

	printf("SERVER STARTED...........\n");

 	int connection_fd;
 	struct sockaddr_in cli_addr;
 	socklen_t clilen;
 	char* s = "buna ziua, stimatule client, 'te dreq";
	while(1){
 		clilen = sizeof(cli_addr);
		connection_fd = accept(socket_fd, (struct sockaddr*)&cli_addr, &clilen);
		printf("%d\n", connection_fd);
		if(connection_fd > 0){
			write(connection_fd, s,  strlen(s));
		} else{
			perror("ana");
		}

		sleep(2);
	}
}