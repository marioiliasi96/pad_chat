#include"stdio.h"  
#include"stdlib.h"  
#include"sys/types.h"  
#include"sys/socket.h"  
#include"string.h"  
#include"netinet/in.h"  
#include"netdb.h"
#include"pthread.h"
#include"assert.h"

#define PORT 8080 
#define BUF_SIZE 150 

void removeSubstring(char *s,const char *toremove)
{
  while( s=strstr(s,toremove) )
    memmove(s,s+strlen(toremove),1+strlen(s+strlen(toremove)));
}

int GetNumber2(const char *str) {
  int sw=0;
  while (*str) {
    int number;
    if (sscanf(str, "%d", &number) == 1) {
      if(sw==0) return number;
      sw++;
    }
    str++;
  } 
  // No int found
  return -1; 
}

void receiveMessage(int sockfd){
	int ret, x, num=0;
	char msg;
	char temp[10];
	char buffer[BUF_SIZE]; 
	memset(buffer, 0, BUF_SIZE);  
	while(1) {
		memset(buffer,0,strlen(buffer));
		ret = recvfrom(sockfd, buffer, BUF_SIZE, 0, NULL, NULL);  
		printf("unde e ana?");
		if (ret < 0) {  
			printf("Error receiving data!\n");    
		} else {
			x=GetNumber2(buffer);
			//printf("%d\n",x);
			snprintf(temp,10," /SEND %d ", x);
			removeSubstring(buffer, temp);
			num=strlen(buffer);
			fputs(buffer, stdout);
		}  
	}
}

int main(int argc, char**argv) {

	struct sockaddr_in addr, cl_addr;  
	int sockfd, sent;  
	char buffer[BUF_SIZE]; 

	sockfd = socket(AF_INET, SOCK_STREAM, 0);  
	if (sockfd < 0) {  
		printf("Error creating socket!\n");  
		exit(1);  
	}
	printf("Socket created...\n");   

	memset(&addr, 0, sizeof(addr));  
	addr.sin_family = AF_INET;  
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = htons(PORT);     

	sent = connect(sockfd, (struct sockaddr *) &addr, sizeof(addr));  
	if (sent < 0) {  
		printf("Error connecting to the server!\n");  
		exit(-1);  
	}

	char sizeBuf[5];
	char newBuf[BUF_SIZE]="";
	int fd = fork();
	if(fd < 0){
		perror("failed fork");
		exit(-1);
	}
	printf("%d\n", fd);
	if(fd == 0){
		printf("adaada");
		receiveMessage(sockfd);
		exit(0);
	}
	exit(0);
	while (fgets(buffer, BUF_SIZE, stdin) != NULL) {
	    if(buffer[0]=='\\')
	    {
	        sent = sendto(sockfd, buffer, BUF_SIZE, 0, (struct sockaddr *) &addr, sizeof(addr));  
	    }
	    else{
		    strcpy(newBuf,"/SEND ");
		    sprintf(sizeBuf, "%d ", strlen(buffer));
		    strcat(newBuf, sizeBuf);
		    strcat(newBuf, buffer);
		    strcat(newBuf, "\0\r\n");
		    if(strlen(buffer)>150)
		    {
		        printf("Error! Message longer than 150 characters\n");
		    }
		    else{
		        sent = sendto(sockfd, newBuf, BUF_SIZE, 0, (struct sockaddr *) &addr, sizeof(addr));  
		    }
		}

		if (sent < 0) {  
			printf("Error sending data!\n\t-%s", buffer);  
		}
	}

	close(sockfd);

}